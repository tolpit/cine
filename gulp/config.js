var app = './public';
var dest = './public/dist';

module.exports = {
    browserSync: {
        watchTask: true,
        notify: true,
        proxy: 'test.dev',
		open: true,
		ghostMode: false
    },
    css: {
        src: app + "/src/app.css",
        dest: dest + "/",
        all: app + "/src/**/*.css"
    },
    fonts: {
        src: app + "/font/**/*",
        dest: dest + "/font"
    },
    images: {
        src: app + "/images/**/*",
        dest: dest + "/images"
    },
    audio: {
        src: app + "/audio/**/*",
        dest: dest + "/audio"
    },
    videos: {
        src: app + "/videos/**/*",
        dest: dest + "/videos"
    },
    svg: {
        src: app + "/src/countries/flags/*.svg",
        dest: dest + "/svg"
    },
    javascript: {
        src: app + "/src/**/*.js",
        dest: dest + "/"
    },
    views:{
        src: app + "/src/**/*.html",
        dest: dest + "/views"
    },
    datas: {
        src: app + "/datas/**/*",
        dest: dest + "/datas"
    },
    surge: {
        mail: "baes.theo@gmail.com",
        domain: "paltry-jewel"
    }
};