var gulp = require('gulp');
var config = require('../config');


gulp.task('watch', [], function() {
	gulp.watch(config.css.all, ['css']);
	gulp.watch(config.javascript.src, ['js']);
	gulp.watch(config.views.src, ['views']);
	gulp.watch(config.svg.src, ['svg']);
});