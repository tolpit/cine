var gulp = require('gulp');
var config = require('../config').javascript;
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');
var stripDebug = require('gulp-strip-debug');
var browserSync = require('browser-sync');

gulp.task('js', function() {
	return gulp.src(config.src)
		.pipe(plumber())
		.pipe(ngAnnotate())
		.pipe(sourcemaps.init())
		.pipe(concat('bundle.js'))
		.pipe(sourcemaps.write())
		//.pipe(stripDebug())
		//.pipe(uglify())
		.pipe(gulp.dest(config.dest))
		.pipe(browserSync.reload({
			stream: true
		}));
});