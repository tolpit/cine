var gulp = require('gulp');
var svgstore = require('gulp-svgstore');
var svgmin = require('gulp-svgmin');
var path = require('path');
var rename = require("gulp-rename");
var config = require('../config').svg;

gulp.task('svg-category', function () {
	return gulp
		.src('./public/src/category/img/*.svg')
		.pipe(svgmin(function (file) {
			var prefix = path.basename(file.relative, path.extname(file.relative));
			return {
				plugins: [{
					removeDoctype: true,

					cleanupIDs: {
						minify: true
					}
				}]
			}
		}))
		.pipe(svgstore())
		.pipe(rename('category.svg'))
		.pipe(gulp.dest(config.dest));
});