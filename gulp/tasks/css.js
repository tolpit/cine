var config 		= require('../config').css;
var gulp        = require("gulp");
var rename      = require("gulp-rename");
var cssnext     = require('gulp-cssnext');
var browserSync = require('browser-sync');

gulp.task('css', function() {
	gulp.src(config.src)
		.pipe(cssnext({
			compress: true
		}))
		.pipe(rename('bundle.css'))
		.pipe(gulp.dest(config.dest))
		.pipe(browserSync.reload({
			stream: true
		}));
});