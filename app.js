var express = require('express');
var path = require('path');
var errorHandler 	= require('errorhandler');
var logger          = require('morgan');
var app = express();


/**
 * Express configuration.
 */
app.set('views', path.join(__dirname, 'server', 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/scripts', express.static(__dirname + '/node_modules'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));

/**
 * Headers
 */
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


/**
 * Router
 */
var router = require('./server/router')(app);

/**
 * Error Handler.
 */
app.use(errorHandler());

var server = app.listen(1337, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
});
