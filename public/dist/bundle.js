"use strict";

var app = angular
    .module('open', [
        'movie',
        'category',
        'timeline',
        'countries',
        'legends',
        'ngSlider',
        'ui.router',
        'ngAnimate',
        'ngResource'
    ]);

app.run(["$state", "$rootScope", "$location", function($state, $rootScope, $location) {
    $rootScope.$state    = $state;
    $rootScope.$location = $location;
}]);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/");

    //Routes
    $stateProvider
        .state('main', {
            url: '/',
            templateUrl: '/dist/views/category/category.list.html',
            controller: 'CategoryCtrl'
        })
        .state('movieList', {
            url: '/category/:category',
            templateUrl: '/dist/views/movie/movie.list.html',
            controller: 'MovieListCtrl'
        });

    $locationProvider.html5Mode(true);
}]);


var originData = {};

angular.module('category', []).
    controller('CategoryCtrl', ["$rootScope", "$scope", "$filter", "Movie", function ($rootScope, $scope, $filter, Movie) {
        $rootScope.state = 'home';

        $scope.years = [];
        $scope.genres = {};
        $scope.genresNames = [];
        $scope.total = 0;

        Movie.all().then((movies) => {

            movies
                .forEach(function(movie) {
                    var year = parseInt(movie.year, 10);

                    if($scope.years.indexOf(year) == -1 && !isNaN(year)) $scope.years.push(year);

                    movie.genres.forEach(function(genre) {
                        if(!$scope.genres[genre]) $scope.genres[genre] = [];

                        if($scope.genresNames.indexOf(genre) == -1) $scope.genresNames.push(genre);
                        $scope.genres[genre].push(movie);
                        $scope.total++;
                    });

                });

            originData = JSON.parse(JSON.stringify($scope.genres));

            $scope.years.sort();

            $scope.$apply();

            $rootScope.$emit('movies:set', {
                years: $scope.years
            });

            setTimeout(() => {
                $rootScope.state = 'categories';
                $rootScope.$apply();
            }, 3000);

            $rootScope.$on('movies:yearRange', (event, range) => {
                $scope.filterRange = range;
                orderMovies();
            });

            $rootScope.$on('movies:country', (event, data) => {
                $scope.filterCountry = ($scope.filterCountry && data.country == $scope.filterCountry) ? null : data.country;

                orderMovies();

                $scope.$apply();
            });

            function orderMovies() {
                var tmp = JSON.parse(JSON.stringify(originData));

                if($scope.filterRange) {
                    for(var genreName in $scope.genres) {
                        if(tmp[genreName] && Array.isArray(tmp[genreName])) {
                            $scope.genres[genreName] = tmp[genreName].filter((movie) => {
                                return movie.year >= $scope.filterRange[0] && movie.year <= $scope.filterRange[1];
                            });
                        }
                    }
                }
                else $scope.genres = tmp;

                if($scope.filterCountry) {
                    for(var genreName in $scope.genres) {
                        $scope.genres[genreName] = $scope.genres[genreName].filter((movie) => {
                            return ~movie.countries.indexOf($scope.filterCountry);
                        });
                    }
                }

                //Calc the total again
                $scope.total = 0;
                for(var genreName in $scope.genres) {
                   $scope.total += $scope.genres[genreName].length;
                }

                //$scope.$apply();
            }
        });

    }]);

/**
 * Created by Théo on 24/11/2015.
 */

var max = 350 / 2;
var size;

angular.module('category')
        .directive('groupCategory', function() {
           return {
               restrict: 'A',
               link: function(scope, element, attrs) {

                   var wrapper = element[0].querySelector('.category-wrapper');
                   var background = element[0].querySelector('.category-background');
                   var picto = element[0].querySelector('.category-picto');
                   var text = element[0].querySelector('.category-name');

                   attrs.$observe('size', function(value){
                       var rayon = (value * 350);
                       var circleSize = (rayon * 2) + 20;

                       if(isNaN(rayon)) return;

                       if(rayon > max) {
                           rayon = max;
                           circleSize = max * 2;
                       }

                       d3.select(wrapper)
                           .transition()
                           .ease('linear')
                           .duration(200)
                           .style("width", () => {
                               return circleSize + "px"
                           })
                           .style("height", () => {
                               return circleSize + "px"
                           });

                       d3.select(background)
                           .transition()
                           .ease('linear')
                           .duration(200)
                           .style("width", () => {
                               return circleSize + "px"
                           })
                           .style("height", () => {
                               return circleSize + "px"
                           });

                       d3.select(picto)
                           .transition()
                           .ease('linear')
                           .duration(200)
                           .style("opacity", () => {
                               return (circleSize < 40 ? 0 : 1);
                           });

                       d3.select(text)
                           .transition()
                           .ease('linear')
                           .duration(200)
                           .style("display", () => {
                               return (circleSize > 40 ? "none" : null);
                           });
                   });

               }
           };
        })
        .directive('audio', ["$rootScope", function($rootScope) {
            return {
                restrict: 'E',
                link: function(scope, element, attrs) {
                    var audioElement = element[0];
                    var step         = 3000 / 5;

                    audioElement.volume = 0.25;

                    $rootScope.$on('movies:set', () => {
                        if($rootScope.state != 'home') return;

                        for(var i = 0; i < 4; i++) {
                            setTimeout(() => {
                                audioElement.currentTime = 0;
                                audioElement.play();
                            }, (step * i) + 500);
                        }
                    });
                }
            };
        }]);
/**
 * Created by Théo on 25/11/2015.
 */

var phrases = {
    "Drama": "A Drama Queen",
    "Comedy": "Very Funny",
    "Romance": "Romantic",
    "Sci-Fi": "An Alien",
    "Action": "An Adventure",
    "Thriller": "A Criminal",
    "War": "A Warrior",
    "Animation": "A Carton",
    "Mystery": "Mysterious",
    "History": "Historical",
    "Sport": "Sporty",
    "Horror": "A Monster",
    "Documentary": "",
    "Music": "A Musician",
    "Western": "A Cow-Boy",
    "Adult": "Érotica"
};

var colors = {
    "Drama": ["#8098B7", "#325075"],
    "Comedy": ["#edd17a", "#B29D5C"],
    "Romance": ["#F88BBB", "#B26487"],
    "Sci-Fi": ["#90ff9e", "#6FC485"],
    "Action": ["#ed9c82", "#B27562"],
    "Thriller": ["#FF5D5D", "#B04040"],
    "War": ["#d14858", "#B23D4B"],
    "Animation": ["#bbe8ea", "#728D8E"],
    "Mystery": ["#8973ff", "#584AA3"],
    "History": ["#3b62ff", "#4a5a98"],
    "Sport": ["#faf6ce", "#BAB799"],
    "Horror": ["#9c9bb5", "#656475"],
    "Documentary": ["#bfd5ff", "#BFD5FF"],
    "Music": ["#43ddc0", "#309D89"],
    "Western": ["#ffffd4", "#854c48"],
    "Adult": ["#e362ff", "#CC58E5"]
};

angular.module('category')
    .filter('yearFilter', () => {
        return (genres, range) => {
            console.log(genres);
            if (!angular.isUndefined(genres) && !angular.isUndefined(range) && Object.keys(genres).length > 0) {
                var tmp = {};

                for(var genreName in genres) {
                    tmp[genreName] = [];

                    if(!angular.isUndefined(genres[genreName])) {
                        genres[genreName] = genres[genreName].forEach((movie) => {
                            if(movie.year >= range[0] && movie.year <= range[1]) tmp[genreName].push(movie);
                        });
                    }
                }

                //console.log(tmp);

                return tmp;
            } else {
                return genres;
            }
        }
    })
    .filter('countryFilter', () => {
        return (genres, country) => {
            if (!angular.isUndefined(genres) && !angular.isUndefined(country) && country.length > 0) {
                var tmp = {};

                for(var genreName in genres) {
                    tmp[genreName] = [];

                    if(!angular.isUndefined(genres[genreName])) {
                        genres[genreName] = genres[genreName].forEach((movie) => {
                            if(~movie.countries.indexOf(country)) tmp[genreName].push(movie);
                        });
                    }
                }

                return tmp;
            } else {
                return genres;
            }
        }
    })
    .filter('roundNumber', () => {
        return (nbr) => {

            return  Math.round(nbr * 10000)/100;

        }
    })
    .filter('phrasing', () => {
        return origin => phrases[origin] || origin;
    })
    .filter('colorMin', () => {
        return origin => (colors[origin] && colors[origin][0] ? colors[origin][0] : "#ffffff");
    })
    .filter('colorMax', () => {
        return origin => (colors[origin] && colors[origin][1] ? colors[origin][1] : "#ffffff");
    });
/**
 * Created by Théo on 25/11/2015.
 */

angular.module('countries', [])
    .directive('countries', function() {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: '/dist/views/countries/countries.html',
            controller: ($rootScope, $scope, Movie) => {

                Movie.countries().then((res) => {
                    $scope.countries = res;
                    $scope.$apply();
                });
                //Watchers
            },
            link: (scope, element, attrs) => {

            }
        }
    })
    .directive('country', ["$rootScope", function($rootScope) {
       return {
           restrict: 'A',
           link: (scope, element, attrs) => {
                element.on('click', function() {
                    $rootScope.$emit('movies:country', { country: attrs.country });
                });
           }
       }
    }])
    .directive('flag', ["$rootScope", function($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                flag: '@flag'
            },
            template: `<i class="flag flag--{{ flag }}" style="background-image: url(/src/countries/flags/flag_{{ flag }}.svg);"></i>`
        }
    }]);
/**
 * Created by Théo on 25/11/2015.
 */

angular.module('countries')
    .filter('countryClass', () => {
        return (country) => {
            return country.toLowerCase().replace(' ', '').replace('-', '');
        }
    });
/**
 * Created by Théo on 25/11/2015.
 */

angular.module('legends', [])
    .directive('legends', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/dist/views/legends/legends.html',
            controller: ($rootScope, $scope) => {



            },
            link: (scope, element, attrs) => {

            }
        }
    });
/**
 * Created by Théo on 25/11/2015.
 */

angular.module('legends')
    .filter('genreClass', () => {
        return (genre) => {
            return genre.replace(' ', '').replace('-', '');
        }
    });

var originData = {};

angular.module('movie', []).
    controller('MovieListCtrl', ["$rootScope", "$scope", "$stateParams", "$location", "Movie", function ($rootScope, $scope, $stateParams, $location, Movie) {
        $rootScope.state = 'keywords';

        $scope.years    = [];
        $scope.category = $stateParams.category;
        $scope.movies   = [];
        $scope.keywords = {};
        $scope.total = 0;

        $scope.filterRange   = $location.$$search.date;
        $scope.filterCountry = $location.$$search.country;

        if($scope.filterRange) $scope.filterRange = $scope.filterRange.replace('[', '').replace(']', '').split(',').map(date => parseInt(date, 10));

        Movie.keywords({ country: $scope.filterCountry, genre: $scope.category }).then((movies) => {

            $scope.movies = movies;

            movies.forEach((movie) => {
                //year
                var year = parseInt(movie.year, 10);

                if($scope.years.indexOf(year) == -1 && !isNaN(year)) $scope.years.push(year);

                //keywords
                if(movie.keywords) {
                    movie.keywords.forEach((keyword) => {
                        if(!$scope.keywords[keyword]) $scope.keywords[keyword] = [];

                        $scope.keywords[keyword].push(movie);
                        $scope.total++;
                    });
                }
            });

            originData = JSON.parse(JSON.stringify($scope.keywords));

            orderByDate();

            $scope.years.sort();

            $scope.$apply();

            $rootScope.$emit('movies:set', {
                years: $scope.years
            });
        });

        $rootScope.$on('movies:yearRange', (event, range) => {
            $scope.filterRange = range;
            orderByDate();
        });

        function orderByDate() {
            for(var keyword in originData) {
                if(!originData[keyword] && Array.isArray(originData[keyword])) return;

                $scope.keywords[keyword] = originData[keyword].filter((movie) => {
                    movie.year = parseInt(movie.year, 10);

                    if($scope.filterRange && (movie.year < $scope.filterRange[0] || movie.year > $scope.filterRange[1])) return false;
                    return true;
                });
            }
        }
    }])
    .controller('MovieCtrl', ["$scope", "$stateParams", "Movie", function ($scope, $stateParams, Movie) {
        Movie.get($stateParams.id).then((movies) => {

        });
    }]);

/**
 * Created by Théo on 24/11/2015.
 */

var max;
var size;

angular.module('movie')
        .directive('movie', function() {
           return {
               restrict: 'A',
               link: function(scope, element, attrs) {
                   var fontSize = (attrs.size > 10 ? 10 : attrs.size) + "em";

                   var color = d3.scale
                       .linear()
                       .domain([1, 10]).range([attrs.from, attrs.to]);

                   element.css({ fontSize });

                   d3.select(element[0])
                       .transition()
                       .ease('linear')
                       .duration(200)
                       .style("fontSize", fontSize)
                       .style("color", () => {
                           return color(attrs.size);
                       });
               }
           };
        });

angular.module('movie').
    factory('Movie', function() {
        return {

            all: () => {
                return new Promise((resolve, reject) => {
                    fetch('/api/movie/list')
                        .then(res => res.json())
                        .then(resolve)
                        .catch(reject);
                });

            },

            category: (name) => {
                return new Promise((resolve, reject) => {
                    fetch('/api/movie/category/' + name)
                        .then(res => res.json())
                        .then(resolve)
                        .catch(reject);
                });
            },

            countries: () => {
                return new Promise((resolve, reject) => {
                    fetch('/api/movie/countries')
                        .then(res => res.json())
                        .then(resolve)
                        .catch(reject);
                });
            },

            keywords: (params) => {
                return new Promise((resolve, reject) => {
                    fetch('/api/movie/keywords?country=' + params.country + '&genre=' + params.genre)
                        .then(res => res.json())
                        .then(resolve)
                        .catch(reject);
                });
            },

            get: (id) => {

            }

        }
    });
/**
 * Created by Théo on 25/11/2015.
 */

angular.module('timeline', [])
    .directive('timeline', function() {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/dist/views/timeline/timeline.html',
            controller: ($rootScope, $scope) => {
                $scope.range       = { value: 0 };
                $scope.range.data  = [0, 1];

                $scope.range.timeline = [];

                //Create the values for the timeline
                for(var i = 1895; i <= 2015; i += 10) $scope.range.timeline.push(i);

                $rootScope.$on('movies:set', (event, data) => {
                    $scope.range.data = data.years;
                    $scope.range.min  = 1895;//data.years[0] > 1895 ? 1895 : data.years[0];
                    $scope.range.max  = data.years[data.years.length - 1];
                    $scope.range.max  = $scope.range.max > 2005 ? 2005 : $scope.range.max;

                    //default value
                    $scope.range.value = 1965;

                    $scope.$apply();
                });

                $scope.changeRange = () => {
                    var value = parseInt($scope.range.value, 10);

                    if(!$scope.init) $scope.init = true;

                    $scope.range.rangeValue = [value, value + 10];
                    $rootScope.$emit('movies:yearRange', $scope.range.rangeValue);
                };

                //Watchers
            },
            link: (scope, element, attrs) => {
                element.addClass('timeline');

                scope.$watch('range.value', function() {
                    if(parseInt(scope.range.value, 10) !== parseInt(element.find('input').val(), 10)) {
                        element.find('input').val(scope.range.value);

                        // :(
                        setTimeout(() => {
                            scope.changeRange();
                            scope.$apply();
                        }, 0);
                    }
                }, true);
            }
        }
    });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyIsImNhdGVnb3J5L2NhdGVnb3J5LmNvbnRyb2xsZXIuanMiLCJjYXRlZ29yeS9jYXRlZ29yeS5kaXJlY3RpdmUuanMiLCJjYXRlZ29yeS9jYXRlZ29yeS5maWx0ZXIuanMiLCJjb3VudHJpZXMvY291bnRyaWVzLmRpcmVjdGl2ZS5qcyIsImNvdW50cmllcy9jb3VudHJpZXMuZmlsdGVyLmpzIiwibGVnZW5kcy9sZWdlbmRzLmRpcmVjdGl2ZS5qcyIsImxlZ2VuZHMvbGVnZW5kcy5maWx0ZXIuanMiLCJtb3ZpZS9tb3ZpZS5jb250cm9sbGVyLmpzIiwibW92aWUvbW92aWUuZGlyZWN0aXZlLmpzIiwibW92aWUvbW92aWUuc2VydmljZS5qcyIsInRpbWVsaW5lL3RpbWVsaW5lLmRpcmVjdGl2ZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN0Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzNGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM1RkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3ZHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDM0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ25CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDM0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDOUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQy9DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xyXG5cclxudmFyIGFwcCA9IGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoJ29wZW4nLCBbXHJcbiAgICAgICAgJ21vdmllJyxcclxuICAgICAgICAnY2F0ZWdvcnknLFxyXG4gICAgICAgICd0aW1lbGluZScsXHJcbiAgICAgICAgJ2NvdW50cmllcycsXHJcbiAgICAgICAgJ2xlZ2VuZHMnLFxyXG4gICAgICAgICduZ1NsaWRlcicsXHJcbiAgICAgICAgJ3VpLnJvdXRlcicsXHJcbiAgICAgICAgJ25nQW5pbWF0ZScsXHJcbiAgICAgICAgJ25nUmVzb3VyY2UnXHJcbiAgICBdKTtcclxuXHJcbmFwcC5ydW4oW1wiJHN0YXRlXCIsIFwiJHJvb3RTY29wZVwiLCBcIiRsb2NhdGlvblwiLCBmdW5jdGlvbigkc3RhdGUsICRyb290U2NvcGUsICRsb2NhdGlvbikge1xyXG4gICAgJHJvb3RTY29wZS4kc3RhdGUgICAgPSAkc3RhdGU7XHJcbiAgICAkcm9vdFNjb3BlLiRsb2NhdGlvbiA9ICRsb2NhdGlvbjtcclxufV0pO1xyXG5cclxuYXBwLmNvbmZpZyhbJyRzdGF0ZVByb3ZpZGVyJywgJyR1cmxSb3V0ZXJQcm92aWRlcicsICckbG9jYXRpb25Qcm92aWRlcicsIGZ1bmN0aW9uKCRzdGF0ZVByb3ZpZGVyLCAkdXJsUm91dGVyUHJvdmlkZXIsICRsb2NhdGlvblByb3ZpZGVyKSB7XHJcbiAgICAkdXJsUm91dGVyUHJvdmlkZXIub3RoZXJ3aXNlKFwiL1wiKTtcclxuXHJcbiAgICAvL1JvdXRlc1xyXG4gICAgJHN0YXRlUHJvdmlkZXJcclxuICAgICAgICAuc3RhdGUoJ21haW4nLCB7XHJcbiAgICAgICAgICAgIHVybDogJy8nLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9kaXN0L3ZpZXdzL2NhdGVnb3J5L2NhdGVnb3J5Lmxpc3QuaHRtbCcsXHJcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdDYXRlZ29yeUN0cmwnXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuc3RhdGUoJ21vdmllTGlzdCcsIHtcclxuICAgICAgICAgICAgdXJsOiAnL2NhdGVnb3J5LzpjYXRlZ29yeScsXHJcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL2Rpc3Qvdmlld3MvbW92aWUvbW92aWUubGlzdC5odG1sJyxcclxuICAgICAgICAgICAgY29udHJvbGxlcjogJ01vdmllTGlzdEN0cmwnXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgJGxvY2F0aW9uUHJvdmlkZXIuaHRtbDVNb2RlKHRydWUpO1xyXG59XSk7XHJcbiIsIlxyXG52YXIgb3JpZ2luRGF0YSA9IHt9O1xyXG5cclxuYW5ndWxhci5tb2R1bGUoJ2NhdGVnb3J5JywgW10pLlxyXG4gICAgY29udHJvbGxlcignQ2F0ZWdvcnlDdHJsJywgW1wiJHJvb3RTY29wZVwiLCBcIiRzY29wZVwiLCBcIiRmaWx0ZXJcIiwgXCJNb3ZpZVwiLCBmdW5jdGlvbiAoJHJvb3RTY29wZSwgJHNjb3BlLCAkZmlsdGVyLCBNb3ZpZSkge1xyXG4gICAgICAgICRyb290U2NvcGUuc3RhdGUgPSAnaG9tZSc7XHJcblxyXG4gICAgICAgICRzY29wZS55ZWFycyA9IFtdO1xyXG4gICAgICAgICRzY29wZS5nZW5yZXMgPSB7fTtcclxuICAgICAgICAkc2NvcGUuZ2VucmVzTmFtZXMgPSBbXTtcclxuICAgICAgICAkc2NvcGUudG90YWwgPSAwO1xyXG5cclxuICAgICAgICBNb3ZpZS5hbGwoKS50aGVuKChtb3ZpZXMpID0+IHtcclxuXHJcbiAgICAgICAgICAgIG1vdmllc1xyXG4gICAgICAgICAgICAgICAgLmZvckVhY2goZnVuY3Rpb24obW92aWUpIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgeWVhciA9IHBhcnNlSW50KG1vdmllLnllYXIsIDEwKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYoJHNjb3BlLnllYXJzLmluZGV4T2YoeWVhcikgPT0gLTEgJiYgIWlzTmFOKHllYXIpKSAkc2NvcGUueWVhcnMucHVzaCh5ZWFyKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgbW92aWUuZ2VucmVzLmZvckVhY2goZnVuY3Rpb24oZ2VucmUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoISRzY29wZS5nZW5yZXNbZ2VucmVdKSAkc2NvcGUuZ2VucmVzW2dlbnJlXSA9IFtdO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoJHNjb3BlLmdlbnJlc05hbWVzLmluZGV4T2YoZ2VucmUpID09IC0xKSAkc2NvcGUuZ2VucmVzTmFtZXMucHVzaChnZW5yZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5nZW5yZXNbZ2VucmVdLnB1c2gobW92aWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUudG90YWwrKztcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIG9yaWdpbkRhdGEgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KCRzY29wZS5nZW5yZXMpKTtcclxuXHJcbiAgICAgICAgICAgICRzY29wZS55ZWFycy5zb3J0KCk7XHJcblxyXG4gICAgICAgICAgICAkc2NvcGUuJGFwcGx5KCk7XHJcblxyXG4gICAgICAgICAgICAkcm9vdFNjb3BlLiRlbWl0KCdtb3ZpZXM6c2V0Jywge1xyXG4gICAgICAgICAgICAgICAgeWVhcnM6ICRzY29wZS55ZWFyc1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgJHJvb3RTY29wZS5zdGF0ZSA9ICdjYXRlZ29yaWVzJztcclxuICAgICAgICAgICAgICAgICRyb290U2NvcGUuJGFwcGx5KCk7XHJcbiAgICAgICAgICAgIH0sIDMwMDApO1xyXG5cclxuICAgICAgICAgICAgJHJvb3RTY29wZS4kb24oJ21vdmllczp5ZWFyUmFuZ2UnLCAoZXZlbnQsIHJhbmdlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAkc2NvcGUuZmlsdGVyUmFuZ2UgPSByYW5nZTtcclxuICAgICAgICAgICAgICAgIG9yZGVyTW92aWVzKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgJHJvb3RTY29wZS4kb24oJ21vdmllczpjb3VudHJ5JywgKGV2ZW50LCBkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAkc2NvcGUuZmlsdGVyQ291bnRyeSA9ICgkc2NvcGUuZmlsdGVyQ291bnRyeSAmJiBkYXRhLmNvdW50cnkgPT0gJHNjb3BlLmZpbHRlckNvdW50cnkpID8gbnVsbCA6IGRhdGEuY291bnRyeTtcclxuXHJcbiAgICAgICAgICAgICAgICBvcmRlck1vdmllcygpO1xyXG5cclxuICAgICAgICAgICAgICAgICRzY29wZS4kYXBwbHkoKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBvcmRlck1vdmllcygpIHtcclxuICAgICAgICAgICAgICAgIHZhciB0bXAgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KG9yaWdpbkRhdGEpKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZigkc2NvcGUuZmlsdGVyUmFuZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGdlbnJlTmFtZSBpbiAkc2NvcGUuZ2VucmVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHRtcFtnZW5yZU5hbWVdICYmIEFycmF5LmlzQXJyYXkodG1wW2dlbnJlTmFtZV0pKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuZ2VucmVzW2dlbnJlTmFtZV0gPSB0bXBbZ2VucmVOYW1lXS5maWx0ZXIoKG1vdmllKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG1vdmllLnllYXIgPj0gJHNjb3BlLmZpbHRlclJhbmdlWzBdICYmIG1vdmllLnllYXIgPD0gJHNjb3BlLmZpbHRlclJhbmdlWzFdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlICRzY29wZS5nZW5yZXMgPSB0bXA7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYoJHNjb3BlLmZpbHRlckNvdW50cnkpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGdlbnJlTmFtZSBpbiAkc2NvcGUuZ2VucmVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5nZW5yZXNbZ2VucmVOYW1lXSA9ICRzY29wZS5nZW5yZXNbZ2VucmVOYW1lXS5maWx0ZXIoKG1vdmllKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gfm1vdmllLmNvdW50cmllcy5pbmRleE9mKCRzY29wZS5maWx0ZXJDb3VudHJ5KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vQ2FsYyB0aGUgdG90YWwgYWdhaW5cclxuICAgICAgICAgICAgICAgICRzY29wZS50b3RhbCA9IDA7XHJcbiAgICAgICAgICAgICAgICBmb3IodmFyIGdlbnJlTmFtZSBpbiAkc2NvcGUuZ2VucmVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAkc2NvcGUudG90YWwgKz0gJHNjb3BlLmdlbnJlc1tnZW5yZU5hbWVdLmxlbmd0aDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyRzY29wZS4kYXBwbHkoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1dKTtcclxuIiwiLyoqXHJcbiAqIENyZWF0ZWQgYnkgVGjDqW8gb24gMjQvMTEvMjAxNS5cclxuICovXHJcblxyXG52YXIgbWF4ID0gMzUwIC8gMjtcclxudmFyIHNpemU7XHJcblxyXG5hbmd1bGFyLm1vZHVsZSgnY2F0ZWdvcnknKVxyXG4gICAgICAgIC5kaXJlY3RpdmUoJ2dyb3VwQ2F0ZWdvcnknLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICByZXN0cmljdDogJ0EnLFxyXG4gICAgICAgICAgICAgICBsaW5rOiBmdW5jdGlvbihzY29wZSwgZWxlbWVudCwgYXR0cnMpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICB2YXIgd3JhcHBlciA9IGVsZW1lbnRbMF0ucXVlcnlTZWxlY3RvcignLmNhdGVnb3J5LXdyYXBwZXInKTtcclxuICAgICAgICAgICAgICAgICAgIHZhciBiYWNrZ3JvdW5kID0gZWxlbWVudFswXS5xdWVyeVNlbGVjdG9yKCcuY2F0ZWdvcnktYmFja2dyb3VuZCcpO1xyXG4gICAgICAgICAgICAgICAgICAgdmFyIHBpY3RvID0gZWxlbWVudFswXS5xdWVyeVNlbGVjdG9yKCcuY2F0ZWdvcnktcGljdG8nKTtcclxuICAgICAgICAgICAgICAgICAgIHZhciB0ZXh0ID0gZWxlbWVudFswXS5xdWVyeVNlbGVjdG9yKCcuY2F0ZWdvcnktbmFtZScpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgIGF0dHJzLiRvYnNlcnZlKCdzaXplJywgZnVuY3Rpb24odmFsdWUpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgIHZhciByYXlvbiA9ICh2YWx1ZSAqIDM1MCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNpcmNsZVNpemUgPSAocmF5b24gKiAyKSArIDIwO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICBpZihpc05hTihyYXlvbikpIHJldHVybjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgaWYocmF5b24gPiBtYXgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF5b24gPSBtYXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGNpcmNsZVNpemUgPSBtYXggKiAyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgZDMuc2VsZWN0KHdyYXBwZXIpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC50cmFuc2l0aW9uKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgLmVhc2UoJ2xpbmVhcicpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC5kdXJhdGlvbigyMDApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdHlsZShcIndpZHRoXCIsICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBjaXJjbGVTaXplICsgXCJweFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdHlsZShcImhlaWdodFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gY2lyY2xlU2l6ZSArIFwicHhcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgZDMuc2VsZWN0KGJhY2tncm91bmQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC50cmFuc2l0aW9uKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgLmVhc2UoJ2xpbmVhcicpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC5kdXJhdGlvbigyMDApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdHlsZShcIndpZHRoXCIsICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBjaXJjbGVTaXplICsgXCJweFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdHlsZShcImhlaWdodFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gY2lyY2xlU2l6ZSArIFwicHhcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgZDMuc2VsZWN0KHBpY3RvKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAudHJhbnNpdGlvbigpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC5lYXNlKCdsaW5lYXInKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAuZHVyYXRpb24oMjAwKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAuc3R5bGUoXCJvcGFjaXR5XCIsICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAoY2lyY2xlU2l6ZSA8IDQwID8gMCA6IDEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgZDMuc2VsZWN0KHRleHQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC50cmFuc2l0aW9uKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgLmVhc2UoJ2xpbmVhcicpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC5kdXJhdGlvbigyMDApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdHlsZShcImRpc3BsYXlcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChjaXJjbGVTaXplID4gNDAgPyBcIm5vbmVcIiA6IG51bGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgIH07XHJcbiAgICAgICAgfSlcclxuICAgICAgICAuZGlyZWN0aXZlKCdhdWRpbycsIFtcIiRyb290U2NvcGVcIiwgZnVuY3Rpb24oJHJvb3RTY29wZSkge1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcclxuICAgICAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uKHNjb3BlLCBlbGVtZW50LCBhdHRycykge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBhdWRpb0VsZW1lbnQgPSBlbGVtZW50WzBdO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBzdGVwICAgICAgICAgPSAzMDAwIC8gNTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgYXVkaW9FbGVtZW50LnZvbHVtZSA9IDAuMjU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJG9uKCdtb3ZpZXM6c2V0JywgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZigkcm9vdFNjb3BlLnN0YXRlICE9ICdob21lJykgcmV0dXJuO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yKHZhciBpID0gMDsgaSA8IDQ7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXVkaW9FbGVtZW50LmN1cnJlbnRUaW1lID0gMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdWRpb0VsZW1lbnQucGxheSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgKHN0ZXAgKiBpKSArIDUwMCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XSk7IiwiLyoqXHJcbiAqIENyZWF0ZWQgYnkgVGjDqW8gb24gMjUvMTEvMjAxNS5cclxuICovXHJcblxyXG52YXIgcGhyYXNlcyA9IHtcclxuICAgIFwiRHJhbWFcIjogXCJBIERyYW1hIFF1ZWVuXCIsXHJcbiAgICBcIkNvbWVkeVwiOiBcIlZlcnkgRnVubnlcIixcclxuICAgIFwiUm9tYW5jZVwiOiBcIlJvbWFudGljXCIsXHJcbiAgICBcIlNjaS1GaVwiOiBcIkFuIEFsaWVuXCIsXHJcbiAgICBcIkFjdGlvblwiOiBcIkFuIEFkdmVudHVyZVwiLFxyXG4gICAgXCJUaHJpbGxlclwiOiBcIkEgQ3JpbWluYWxcIixcclxuICAgIFwiV2FyXCI6IFwiQSBXYXJyaW9yXCIsXHJcbiAgICBcIkFuaW1hdGlvblwiOiBcIkEgQ2FydG9uXCIsXHJcbiAgICBcIk15c3RlcnlcIjogXCJNeXN0ZXJpb3VzXCIsXHJcbiAgICBcIkhpc3RvcnlcIjogXCJIaXN0b3JpY2FsXCIsXHJcbiAgICBcIlNwb3J0XCI6IFwiU3BvcnR5XCIsXHJcbiAgICBcIkhvcnJvclwiOiBcIkEgTW9uc3RlclwiLFxyXG4gICAgXCJEb2N1bWVudGFyeVwiOiBcIlwiLFxyXG4gICAgXCJNdXNpY1wiOiBcIkEgTXVzaWNpYW5cIixcclxuICAgIFwiV2VzdGVyblwiOiBcIkEgQ293LUJveVwiLFxyXG4gICAgXCJBZHVsdFwiOiBcIsOJcm90aWNhXCJcclxufTtcclxuXHJcbnZhciBjb2xvcnMgPSB7XHJcbiAgICBcIkRyYW1hXCI6IFtcIiM4MDk4QjdcIiwgXCIjMzI1MDc1XCJdLFxyXG4gICAgXCJDb21lZHlcIjogW1wiI2VkZDE3YVwiLCBcIiNCMjlENUNcIl0sXHJcbiAgICBcIlJvbWFuY2VcIjogW1wiI0Y4OEJCQlwiLCBcIiNCMjY0ODdcIl0sXHJcbiAgICBcIlNjaS1GaVwiOiBbXCIjOTBmZjllXCIsIFwiIzZGQzQ4NVwiXSxcclxuICAgIFwiQWN0aW9uXCI6IFtcIiNlZDljODJcIiwgXCIjQjI3NTYyXCJdLFxyXG4gICAgXCJUaHJpbGxlclwiOiBbXCIjRkY1RDVEXCIsIFwiI0IwNDA0MFwiXSxcclxuICAgIFwiV2FyXCI6IFtcIiNkMTQ4NThcIiwgXCIjQjIzRDRCXCJdLFxyXG4gICAgXCJBbmltYXRpb25cIjogW1wiI2JiZThlYVwiLCBcIiM3MjhEOEVcIl0sXHJcbiAgICBcIk15c3RlcnlcIjogW1wiIzg5NzNmZlwiLCBcIiM1ODRBQTNcIl0sXHJcbiAgICBcIkhpc3RvcnlcIjogW1wiIzNiNjJmZlwiLCBcIiM0YTVhOThcIl0sXHJcbiAgICBcIlNwb3J0XCI6IFtcIiNmYWY2Y2VcIiwgXCIjQkFCNzk5XCJdLFxyXG4gICAgXCJIb3Jyb3JcIjogW1wiIzljOWJiNVwiLCBcIiM2NTY0NzVcIl0sXHJcbiAgICBcIkRvY3VtZW50YXJ5XCI6IFtcIiNiZmQ1ZmZcIiwgXCIjQkZENUZGXCJdLFxyXG4gICAgXCJNdXNpY1wiOiBbXCIjNDNkZGMwXCIsIFwiIzMwOUQ4OVwiXSxcclxuICAgIFwiV2VzdGVyblwiOiBbXCIjZmZmZmQ0XCIsIFwiIzg1NGM0OFwiXSxcclxuICAgIFwiQWR1bHRcIjogW1wiI2UzNjJmZlwiLCBcIiNDQzU4RTVcIl1cclxufTtcclxuXHJcbmFuZ3VsYXIubW9kdWxlKCdjYXRlZ29yeScpXHJcbiAgICAuZmlsdGVyKCd5ZWFyRmlsdGVyJywgKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiAoZ2VucmVzLCByYW5nZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhnZW5yZXMpO1xyXG4gICAgICAgICAgICBpZiAoIWFuZ3VsYXIuaXNVbmRlZmluZWQoZ2VucmVzKSAmJiAhYW5ndWxhci5pc1VuZGVmaW5lZChyYW5nZSkgJiYgT2JqZWN0LmtleXMoZ2VucmVzKS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgdG1wID0ge307XHJcblxyXG4gICAgICAgICAgICAgICAgZm9yKHZhciBnZW5yZU5hbWUgaW4gZ2VucmVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG1wW2dlbnJlTmFtZV0gPSBbXTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYoIWFuZ3VsYXIuaXNVbmRlZmluZWQoZ2VucmVzW2dlbnJlTmFtZV0pKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdlbnJlc1tnZW5yZU5hbWVdID0gZ2VucmVzW2dlbnJlTmFtZV0uZm9yRWFjaCgobW92aWUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKG1vdmllLnllYXIgPj0gcmFuZ2VbMF0gJiYgbW92aWUueWVhciA8PSByYW5nZVsxXSkgdG1wW2dlbnJlTmFtZV0ucHVzaChtb3ZpZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKHRtcCk7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRtcDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBnZW5yZXM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9KVxyXG4gICAgLmZpbHRlcignY291bnRyeUZpbHRlcicsICgpID0+IHtcclxuICAgICAgICByZXR1cm4gKGdlbnJlcywgY291bnRyeSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIWFuZ3VsYXIuaXNVbmRlZmluZWQoZ2VucmVzKSAmJiAhYW5ndWxhci5pc1VuZGVmaW5lZChjb3VudHJ5KSAmJiBjb3VudHJ5Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHZhciB0bXAgPSB7fTtcclxuXHJcbiAgICAgICAgICAgICAgICBmb3IodmFyIGdlbnJlTmFtZSBpbiBnZW5yZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICB0bXBbZ2VucmVOYW1lXSA9IFtdO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZighYW5ndWxhci5pc1VuZGVmaW5lZChnZW5yZXNbZ2VucmVOYW1lXSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZ2VucmVzW2dlbnJlTmFtZV0gPSBnZW5yZXNbZ2VucmVOYW1lXS5mb3JFYWNoKChtb3ZpZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYofm1vdmllLmNvdW50cmllcy5pbmRleE9mKGNvdW50cnkpKSB0bXBbZ2VucmVOYW1lXS5wdXNoKG1vdmllKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiB0bXA7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZ2VucmVzO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSlcclxuICAgIC5maWx0ZXIoJ3JvdW5kTnVtYmVyJywgKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiAobmJyKSA9PiB7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gIE1hdGgucm91bmQobmJyICogMTAwMDApLzEwMDtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfSlcclxuICAgIC5maWx0ZXIoJ3BocmFzaW5nJywgKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiBvcmlnaW4gPT4gcGhyYXNlc1tvcmlnaW5dIHx8IG9yaWdpbjtcclxuICAgIH0pXHJcbiAgICAuZmlsdGVyKCdjb2xvck1pbicsICgpID0+IHtcclxuICAgICAgICByZXR1cm4gb3JpZ2luID0+IChjb2xvcnNbb3JpZ2luXSAmJiBjb2xvcnNbb3JpZ2luXVswXSA/IGNvbG9yc1tvcmlnaW5dWzBdIDogXCIjZmZmZmZmXCIpO1xyXG4gICAgfSlcclxuICAgIC5maWx0ZXIoJ2NvbG9yTWF4JywgKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiBvcmlnaW4gPT4gKGNvbG9yc1tvcmlnaW5dICYmIGNvbG9yc1tvcmlnaW5dWzFdID8gY29sb3JzW29yaWdpbl1bMV0gOiBcIiNmZmZmZmZcIik7XHJcbiAgICB9KTsiLCIvKipcclxuICogQ3JlYXRlZCBieSBUaMOpbyBvbiAyNS8xMS8yMDE1LlxyXG4gKi9cclxuXHJcbmFuZ3VsYXIubW9kdWxlKCdjb3VudHJpZXMnLCBbXSlcclxuICAgIC5kaXJlY3RpdmUoJ2NvdW50cmllcycsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXHJcbiAgICAgICAgICAgIHJlcGxhY2U6IHRydWUsXHJcbiAgICAgICAgICAgIHNjb3BlOiB0cnVlLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9kaXN0L3ZpZXdzL2NvdW50cmllcy9jb3VudHJpZXMuaHRtbCcsXHJcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICgkcm9vdFNjb3BlLCAkc2NvcGUsIE1vdmllKSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgTW92aWUuY291bnRyaWVzKCkudGhlbigocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmNvdW50cmllcyA9IHJlcztcclxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuJGFwcGx5KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIC8vV2F0Y2hlcnNcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgbGluazogKHNjb3BlLCBlbGVtZW50LCBhdHRycykgPT4ge1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0pXHJcbiAgICAuZGlyZWN0aXZlKCdjb3VudHJ5JywgW1wiJHJvb3RTY29wZVwiLCBmdW5jdGlvbigkcm9vdFNjb3BlKSB7XHJcbiAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgIHJlc3RyaWN0OiAnQScsXHJcbiAgICAgICAgICAgbGluazogKHNjb3BlLCBlbGVtZW50LCBhdHRycykgPT4ge1xyXG4gICAgICAgICAgICAgICAgZWxlbWVudC5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLiRlbWl0KCdtb3ZpZXM6Y291bnRyeScsIHsgY291bnRyeTogYXR0cnMuY291bnRyeSB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgIH1cclxuICAgICAgIH1cclxuICAgIH1dKVxyXG4gICAgLmRpcmVjdGl2ZSgnZmxhZycsIFtcIiRyb290U2NvcGVcIiwgZnVuY3Rpb24oJHJvb3RTY29wZSkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXHJcbiAgICAgICAgICAgIHJlcGxhY2U6IHRydWUsXHJcbiAgICAgICAgICAgIHNjb3BlOiB7XHJcbiAgICAgICAgICAgICAgICBmbGFnOiAnQGZsYWcnXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHRlbXBsYXRlOiBgPGkgY2xhc3M9XCJmbGFnIGZsYWctLXt7IGZsYWcgfX1cIiBzdHlsZT1cImJhY2tncm91bmQtaW1hZ2U6IHVybCgvc3JjL2NvdW50cmllcy9mbGFncy9mbGFnX3t7IGZsYWcgfX0uc3ZnKTtcIj48L2k+YFxyXG4gICAgICAgIH1cclxuICAgIH1dKTsiLCIvKipcclxuICogQ3JlYXRlZCBieSBUaMOpbyBvbiAyNS8xMS8yMDE1LlxyXG4gKi9cclxuXHJcbmFuZ3VsYXIubW9kdWxlKCdjb3VudHJpZXMnKVxyXG4gICAgLmZpbHRlcignY291bnRyeUNsYXNzJywgKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiAoY291bnRyeSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gY291bnRyeS50b0xvd2VyQ2FzZSgpLnJlcGxhY2UoJyAnLCAnJykucmVwbGFjZSgnLScsICcnKTtcclxuICAgICAgICB9XHJcbiAgICB9KTsiLCIvKipcclxuICogQ3JlYXRlZCBieSBUaMOpbyBvbiAyNS8xMS8yMDE1LlxyXG4gKi9cclxuXHJcbmFuZ3VsYXIubW9kdWxlKCdsZWdlbmRzJywgW10pXHJcbiAgICAuZGlyZWN0aXZlKCdsZWdlbmRzJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcclxuICAgICAgICAgICAgcmVwbGFjZTogdHJ1ZSxcclxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvZGlzdC92aWV3cy9sZWdlbmRzL2xlZ2VuZHMuaHRtbCcsXHJcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICgkcm9vdFNjb3BlLCAkc2NvcGUpID0+IHtcclxuXHJcblxyXG5cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgbGluazogKHNjb3BlLCBlbGVtZW50LCBhdHRycykgPT4ge1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0pOyIsIi8qKlxyXG4gKiBDcmVhdGVkIGJ5IFRow6lvIG9uIDI1LzExLzIwMTUuXHJcbiAqL1xyXG5cclxuYW5ndWxhci5tb2R1bGUoJ2xlZ2VuZHMnKVxyXG4gICAgLmZpbHRlcignZ2VucmVDbGFzcycsICgpID0+IHtcclxuICAgICAgICByZXR1cm4gKGdlbnJlKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBnZW5yZS5yZXBsYWNlKCcgJywgJycpLnJlcGxhY2UoJy0nLCAnJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7IiwiXHJcbnZhciBvcmlnaW5EYXRhID0ge307XHJcblxyXG5hbmd1bGFyLm1vZHVsZSgnbW92aWUnLCBbXSkuXHJcbiAgICBjb250cm9sbGVyKCdNb3ZpZUxpc3RDdHJsJywgW1wiJHJvb3RTY29wZVwiLCBcIiRzY29wZVwiLCBcIiRzdGF0ZVBhcmFtc1wiLCBcIiRsb2NhdGlvblwiLCBcIk1vdmllXCIsIGZ1bmN0aW9uICgkcm9vdFNjb3BlLCAkc2NvcGUsICRzdGF0ZVBhcmFtcywgJGxvY2F0aW9uLCBNb3ZpZSkge1xyXG4gICAgICAgICRyb290U2NvcGUuc3RhdGUgPSAna2V5d29yZHMnO1xyXG5cclxuICAgICAgICAkc2NvcGUueWVhcnMgICAgPSBbXTtcclxuICAgICAgICAkc2NvcGUuY2F0ZWdvcnkgPSAkc3RhdGVQYXJhbXMuY2F0ZWdvcnk7XHJcbiAgICAgICAgJHNjb3BlLm1vdmllcyAgID0gW107XHJcbiAgICAgICAgJHNjb3BlLmtleXdvcmRzID0ge307XHJcbiAgICAgICAgJHNjb3BlLnRvdGFsID0gMDtcclxuXHJcbiAgICAgICAgJHNjb3BlLmZpbHRlclJhbmdlICAgPSAkbG9jYXRpb24uJCRzZWFyY2guZGF0ZTtcclxuICAgICAgICAkc2NvcGUuZmlsdGVyQ291bnRyeSA9ICRsb2NhdGlvbi4kJHNlYXJjaC5jb3VudHJ5O1xyXG5cclxuICAgICAgICBpZigkc2NvcGUuZmlsdGVyUmFuZ2UpICRzY29wZS5maWx0ZXJSYW5nZSA9ICRzY29wZS5maWx0ZXJSYW5nZS5yZXBsYWNlKCdbJywgJycpLnJlcGxhY2UoJ10nLCAnJykuc3BsaXQoJywnKS5tYXAoZGF0ZSA9PiBwYXJzZUludChkYXRlLCAxMCkpO1xyXG5cclxuICAgICAgICBNb3ZpZS5rZXl3b3Jkcyh7IGNvdW50cnk6ICRzY29wZS5maWx0ZXJDb3VudHJ5LCBnZW5yZTogJHNjb3BlLmNhdGVnb3J5IH0pLnRoZW4oKG1vdmllcykgPT4ge1xyXG5cclxuICAgICAgICAgICAgJHNjb3BlLm1vdmllcyA9IG1vdmllcztcclxuXHJcbiAgICAgICAgICAgIG1vdmllcy5mb3JFYWNoKChtb3ZpZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgLy95ZWFyXHJcbiAgICAgICAgICAgICAgICB2YXIgeWVhciA9IHBhcnNlSW50KG1vdmllLnllYXIsIDEwKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZigkc2NvcGUueWVhcnMuaW5kZXhPZih5ZWFyKSA9PSAtMSAmJiAhaXNOYU4oeWVhcikpICRzY29wZS55ZWFycy5wdXNoKHllYXIpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8va2V5d29yZHNcclxuICAgICAgICAgICAgICAgIGlmKG1vdmllLmtleXdvcmRzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbW92aWUua2V5d29yZHMuZm9yRWFjaCgoa2V5d29yZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZighJHNjb3BlLmtleXdvcmRzW2tleXdvcmRdKSAkc2NvcGUua2V5d29yZHNba2V5d29yZF0gPSBbXTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5rZXl3b3Jkc1trZXl3b3JkXS5wdXNoKG1vdmllKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnRvdGFsKys7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgb3JpZ2luRGF0YSA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoJHNjb3BlLmtleXdvcmRzKSk7XHJcblxyXG4gICAgICAgICAgICBvcmRlckJ5RGF0ZSgpO1xyXG5cclxuICAgICAgICAgICAgJHNjb3BlLnllYXJzLnNvcnQoKTtcclxuXHJcbiAgICAgICAgICAgICRzY29wZS4kYXBwbHkoKTtcclxuXHJcbiAgICAgICAgICAgICRyb290U2NvcGUuJGVtaXQoJ21vdmllczpzZXQnLCB7XHJcbiAgICAgICAgICAgICAgICB5ZWFyczogJHNjb3BlLnllYXJzXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkcm9vdFNjb3BlLiRvbignbW92aWVzOnllYXJSYW5nZScsIChldmVudCwgcmFuZ2UpID0+IHtcclxuICAgICAgICAgICAgJHNjb3BlLmZpbHRlclJhbmdlID0gcmFuZ2U7XHJcbiAgICAgICAgICAgIG9yZGVyQnlEYXRlKCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIG9yZGVyQnlEYXRlKCkge1xyXG4gICAgICAgICAgICBmb3IodmFyIGtleXdvcmQgaW4gb3JpZ2luRGF0YSkge1xyXG4gICAgICAgICAgICAgICAgaWYoIW9yaWdpbkRhdGFba2V5d29yZF0gJiYgQXJyYXkuaXNBcnJheShvcmlnaW5EYXRhW2tleXdvcmRdKSkgcmV0dXJuO1xyXG5cclxuICAgICAgICAgICAgICAgICRzY29wZS5rZXl3b3Jkc1trZXl3b3JkXSA9IG9yaWdpbkRhdGFba2V5d29yZF0uZmlsdGVyKChtb3ZpZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG1vdmllLnllYXIgPSBwYXJzZUludChtb3ZpZS55ZWFyLCAxMCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmKCRzY29wZS5maWx0ZXJSYW5nZSAmJiAobW92aWUueWVhciA8ICRzY29wZS5maWx0ZXJSYW5nZVswXSB8fCBtb3ZpZS55ZWFyID4gJHNjb3BlLmZpbHRlclJhbmdlWzFdKSkgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XSlcclxuICAgIC5jb250cm9sbGVyKCdNb3ZpZUN0cmwnLCBbXCIkc2NvcGVcIiwgXCIkc3RhdGVQYXJhbXNcIiwgXCJNb3ZpZVwiLCBmdW5jdGlvbiAoJHNjb3BlLCAkc3RhdGVQYXJhbXMsIE1vdmllKSB7XHJcbiAgICAgICAgTW92aWUuZ2V0KCRzdGF0ZVBhcmFtcy5pZCkudGhlbigobW92aWVzKSA9PiB7XHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgfV0pO1xyXG4iLCIvKipcclxuICogQ3JlYXRlZCBieSBUaMOpbyBvbiAyNC8xMS8yMDE1LlxyXG4gKi9cclxuXHJcbnZhciBtYXg7XHJcbnZhciBzaXplO1xyXG5cclxuYW5ndWxhci5tb2R1bGUoJ21vdmllJylcclxuICAgICAgICAuZGlyZWN0aXZlKCdtb3ZpZScsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgIHJlc3RyaWN0OiAnQScsXHJcbiAgICAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uKHNjb3BlLCBlbGVtZW50LCBhdHRycykge1xyXG4gICAgICAgICAgICAgICAgICAgdmFyIGZvbnRTaXplID0gKGF0dHJzLnNpemUgPiAxMCA/IDEwIDogYXR0cnMuc2l6ZSkgKyBcImVtXCI7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgdmFyIGNvbG9yID0gZDMuc2NhbGVcclxuICAgICAgICAgICAgICAgICAgICAgICAubGluZWFyKClcclxuICAgICAgICAgICAgICAgICAgICAgICAuZG9tYWluKFsxLCAxMF0pLnJhbmdlKFthdHRycy5mcm9tLCBhdHRycy50b10pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuY3NzKHsgZm9udFNpemUgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgZDMuc2VsZWN0KGVsZW1lbnRbMF0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgLnRyYW5zaXRpb24oKVxyXG4gICAgICAgICAgICAgICAgICAgICAgIC5lYXNlKCdsaW5lYXInKVxyXG4gICAgICAgICAgICAgICAgICAgICAgIC5kdXJhdGlvbigyMDApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgLnN0eWxlKFwiZm9udFNpemVcIiwgZm9udFNpemUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgLnN0eWxlKFwiY29sb3JcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gY29sb3IoYXR0cnMuc2l6ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICB9O1xyXG4gICAgICAgIH0pOyIsIlxyXG5hbmd1bGFyLm1vZHVsZSgnbW92aWUnKS5cclxuICAgIGZhY3RvcnkoJ01vdmllJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuXHJcbiAgICAgICAgICAgIGFsbDogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBmZXRjaCgnL2FwaS9tb3ZpZS9saXN0JylcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzID0+IHJlcy5qc29uKCkpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc29sdmUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5jYXRjaChyZWplY3QpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgY2F0ZWdvcnk6IChuYW1lKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGZldGNoKCcvYXBpL21vdmllL2NhdGVnb3J5LycgKyBuYW1lKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbihyZXMgPT4gcmVzLmpzb24oKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzb2x2ZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKHJlamVjdCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGNvdW50cmllczogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBmZXRjaCgnL2FwaS9tb3ZpZS9jb3VudHJpZXMnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbihyZXMgPT4gcmVzLmpzb24oKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzb2x2ZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKHJlamVjdCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGtleXdvcmRzOiAocGFyYW1zKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGZldGNoKCcvYXBpL21vdmllL2tleXdvcmRzP2NvdW50cnk9JyArIHBhcmFtcy5jb3VudHJ5ICsgJyZnZW5yZT0nICsgcGFyYW1zLmdlbnJlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbihyZXMgPT4gcmVzLmpzb24oKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzb2x2ZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKHJlamVjdCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIGdldDogKGlkKSA9PiB7XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuICAgIH0pOyIsIi8qKlxyXG4gKiBDcmVhdGVkIGJ5IFRow6lvIG9uIDI1LzExLzIwMTUuXHJcbiAqL1xyXG5cclxuYW5ndWxhci5tb2R1bGUoJ3RpbWVsaW5lJywgW10pXHJcbiAgICAuZGlyZWN0aXZlKCd0aW1lbGluZScsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXHJcbiAgICAgICAgICAgIHNjb3BlOiB0cnVlLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9kaXN0L3ZpZXdzL3RpbWVsaW5lL3RpbWVsaW5lLmh0bWwnLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyOiAoJHJvb3RTY29wZSwgJHNjb3BlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAkc2NvcGUucmFuZ2UgICAgICAgPSB7IHZhbHVlOiAwIH07XHJcbiAgICAgICAgICAgICAgICAkc2NvcGUucmFuZ2UuZGF0YSAgPSBbMCwgMV07XHJcblxyXG4gICAgICAgICAgICAgICAgJHNjb3BlLnJhbmdlLnRpbWVsaW5lID0gW107XHJcblxyXG4gICAgICAgICAgICAgICAgLy9DcmVhdGUgdGhlIHZhbHVlcyBmb3IgdGhlIHRpbWVsaW5lXHJcbiAgICAgICAgICAgICAgICBmb3IodmFyIGkgPSAxODk1OyBpIDw9IDIwMTU7IGkgKz0gMTApICRzY29wZS5yYW5nZS50aW1lbGluZS5wdXNoKGkpO1xyXG5cclxuICAgICAgICAgICAgICAgICRyb290U2NvcGUuJG9uKCdtb3ZpZXM6c2V0JywgKGV2ZW50LCBkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJhbmdlLmRhdGEgPSBkYXRhLnllYXJzO1xyXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5yYW5nZS5taW4gID0gMTg5NTsvL2RhdGEueWVhcnNbMF0gPiAxODk1ID8gMTg5NSA6IGRhdGEueWVhcnNbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJhbmdlLm1heCAgPSBkYXRhLnllYXJzW2RhdGEueWVhcnMubGVuZ3RoIC0gMV07XHJcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJhbmdlLm1heCAgPSAkc2NvcGUucmFuZ2UubWF4ID4gMjAwNSA/IDIwMDUgOiAkc2NvcGUucmFuZ2UubWF4O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvL2RlZmF1bHQgdmFsdWVcclxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUucmFuZ2UudmFsdWUgPSAxOTY1O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuJGFwcGx5KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAkc2NvcGUuY2hhbmdlUmFuZ2UgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIHZhbHVlID0gcGFyc2VJbnQoJHNjb3BlLnJhbmdlLnZhbHVlLCAxMCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmKCEkc2NvcGUuaW5pdCkgJHNjb3BlLmluaXQgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUucmFuZ2UucmFuZ2VWYWx1ZSA9IFt2YWx1ZSwgdmFsdWUgKyAxMF07XHJcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kZW1pdCgnbW92aWVzOnllYXJSYW5nZScsICRzY29wZS5yYW5nZS5yYW5nZVZhbHVlKTtcclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgLy9XYXRjaGVyc1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBsaW5rOiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50LmFkZENsYXNzKCd0aW1lbGluZScpO1xyXG5cclxuICAgICAgICAgICAgICAgIHNjb3BlLiR3YXRjaCgncmFuZ2UudmFsdWUnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZihwYXJzZUludChzY29wZS5yYW5nZS52YWx1ZSwgMTApICE9PSBwYXJzZUludChlbGVtZW50LmZpbmQoJ2lucHV0JykudmFsKCksIDEwKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmZpbmQoJ2lucHV0JykudmFsKHNjb3BlLnJhbmdlLnZhbHVlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIDooXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuY2hhbmdlUmFuZ2UoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLiRhcHBseSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCAwKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCB0cnVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0pOyJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
