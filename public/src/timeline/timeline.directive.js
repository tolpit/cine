/**
 * Created by Théo on 25/11/2015.
 */

angular.module('timeline', [])
    .directive('timeline', function() {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/dist/views/timeline/timeline.html',
            controller: ($rootScope, $scope) => {
                $scope.range       = { value: 0 };
                $scope.range.data  = [0, 1];

                $scope.range.timeline = [];

                //Create the values for the timeline
                for(var i = 1895; i <= 2015; i += 10) $scope.range.timeline.push(i);

                $rootScope.$on('movies:set', (event, data) => {
                    $scope.range.data = data.years;
                    $scope.range.min  = 1895;//data.years[0] > 1895 ? 1895 : data.years[0];
                    $scope.range.max  = data.years[data.years.length - 1];
                    $scope.range.max  = $scope.range.max > 2005 ? 2005 : $scope.range.max;

                    //default value
                    $scope.range.value = 1965;

                    $scope.$apply();
                });

                $scope.changeRange = () => {
                    var value = parseInt($scope.range.value, 10);

                    if(!$scope.init) $scope.init = true;

                    $scope.range.rangeValue = [value, value + 10];
                    $rootScope.$emit('movies:yearRange', $scope.range.rangeValue);
                };

                //Watchers
            },
            link: (scope, element, attrs) => {
                element.addClass('timeline');

                scope.$watch('range.value', function() {
                    if(parseInt(scope.range.value, 10) !== parseInt(element.find('input').val(), 10)) {
                        element.find('input').val(scope.range.value);

                        // :(
                        setTimeout(() => {
                            scope.changeRange();
                            scope.$apply();
                        }, 0);
                    }
                }, true);
            }
        }
    });