"use strict";

var app = angular
    .module('open', [
        'movie',
        'category',
        'timeline',
        'countries',
        'legends',
        'ngSlider',
        'ui.router',
        'ngAnimate',
        'ngResource'
    ]);

app.run(function($state, $rootScope, $location) {
    $rootScope.$state    = $state;
    $rootScope.$location = $location;
});

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/");

    //Routes
    $stateProvider
        .state('main', {
            url: '/',
            templateUrl: '/dist/views/category/category.list.html',
            controller: 'CategoryCtrl'
        })
        .state('movieList', {
            url: '/category/:category',
            templateUrl: '/dist/views/movie/movie.list.html',
            controller: 'MovieListCtrl'
        });

    $locationProvider.html5Mode(true);
}]);
