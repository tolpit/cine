/**
 * Created by Théo on 25/11/2015.
 */

angular.module('legends')
    .filter('genreClass', () => {
        return (genre) => {
            return genre.replace(' ', '').replace('-', '');
        }
    });