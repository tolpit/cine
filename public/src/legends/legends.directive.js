/**
 * Created by Théo on 25/11/2015.
 */

angular.module('legends', [])
    .directive('legends', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/dist/views/legends/legends.html',
            controller: ($rootScope, $scope) => {



            },
            link: (scope, element, attrs) => {

            }
        }
    });