/**
 * Created by Théo on 24/11/2015.
 */

var max = 350 / 2;
var size;

angular.module('category')
        .directive('groupCategory', function() {
           return {
               restrict: 'A',
               link: function(scope, element, attrs) {

                   var wrapper = element[0].querySelector('.category-wrapper');
                   var background = element[0].querySelector('.category-background');
                   var picto = element[0].querySelector('.category-picto');
                   var text = element[0].querySelector('.category-name');

                   attrs.$observe('size', function(value){
                       var rayon = (value * 350);
                       var circleSize = (rayon * 2) + 20;

                       if(isNaN(rayon)) return;

                       if(rayon > max) {
                           rayon = max;
                           circleSize = max * 2;
                       }

                       d3.select(wrapper)
                           .transition()
                           .ease('linear')
                           .duration(200)
                           .style("width", () => {
                               return circleSize + "px"
                           })
                           .style("height", () => {
                               return circleSize + "px"
                           });

                       d3.select(background)
                           .transition()
                           .ease('linear')
                           .duration(200)
                           .style("width", () => {
                               return circleSize + "px"
                           })
                           .style("height", () => {
                               return circleSize + "px"
                           });

                       d3.select(picto)
                           .transition()
                           .ease('linear')
                           .duration(200)
                           .style("opacity", () => {
                               return (circleSize < 40 ? 0 : 1);
                           });

                       d3.select(text)
                           .transition()
                           .ease('linear')
                           .duration(200)
                           .style("display", () => {
                               return (circleSize > 40 ? "none" : null);
                           });
                   });

               }
           };
        })
        .directive('audio', function($rootScope) {
            return {
                restrict: 'E',
                link: function(scope, element, attrs) {
                    var audioElement = element[0];
                    var step         = 3000 / 5;

                    audioElement.volume = 0.25;

                    $rootScope.$on('movies:set', () => {
                        if($rootScope.state != 'home') return;

                        for(var i = 0; i < 4; i++) {
                            setTimeout(() => {
                                audioElement.currentTime = 0;
                                audioElement.play();
                            }, (step * i) + 500);
                        }
                    });
                }
            };
        });