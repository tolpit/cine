/**
 * Created by Théo on 25/11/2015.
 */

var phrases = {
    "Drama": "A Drama Queen",
    "Comedy": "Very Funny",
    "Romance": "Romantic",
    "Sci-Fi": "An Alien",
    "Action": "An Adventure",
    "Thriller": "A Criminal",
    "War": "A Warrior",
    "Animation": "A Carton",
    "Mystery": "Mysterious",
    "History": "Historical",
    "Sport": "Sporty",
    "Horror": "A Monster",
    "Documentary": "",
    "Music": "A Musician",
    "Western": "A Cow-Boy",
    "Adult": "Érotica"
};

var colors = {
    "Drama": ["#8098B7", "#325075"],
    "Comedy": ["#edd17a", "#B29D5C"],
    "Romance": ["#F88BBB", "#B26487"],
    "Sci-Fi": ["#90ff9e", "#6FC485"],
    "Action": ["#ed9c82", "#B27562"],
    "Thriller": ["#FF5D5D", "#B04040"],
    "War": ["#d14858", "#B23D4B"],
    "Animation": ["#bbe8ea", "#728D8E"],
    "Mystery": ["#8973ff", "#584AA3"],
    "History": ["#3b62ff", "#4a5a98"],
    "Sport": ["#faf6ce", "#BAB799"],
    "Horror": ["#9c9bb5", "#656475"],
    "Documentary": ["#bfd5ff", "#BFD5FF"],
    "Music": ["#43ddc0", "#309D89"],
    "Western": ["#ffffd4", "#854c48"],
    "Adult": ["#e362ff", "#CC58E5"]
};

angular.module('category')
    .filter('yearFilter', () => {
        return (genres, range) => {
            console.log(genres);
            if (!angular.isUndefined(genres) && !angular.isUndefined(range) && Object.keys(genres).length > 0) {
                var tmp = {};

                for(var genreName in genres) {
                    tmp[genreName] = [];

                    if(!angular.isUndefined(genres[genreName])) {
                        genres[genreName] = genres[genreName].forEach((movie) => {
                            if(movie.year >= range[0] && movie.year <= range[1]) tmp[genreName].push(movie);
                        });
                    }
                }

                //console.log(tmp);

                return tmp;
            } else {
                return genres;
            }
        }
    })
    .filter('countryFilter', () => {
        return (genres, country) => {
            if (!angular.isUndefined(genres) && !angular.isUndefined(country) && country.length > 0) {
                var tmp = {};

                for(var genreName in genres) {
                    tmp[genreName] = [];

                    if(!angular.isUndefined(genres[genreName])) {
                        genres[genreName] = genres[genreName].forEach((movie) => {
                            if(~movie.countries.indexOf(country)) tmp[genreName].push(movie);
                        });
                    }
                }

                return tmp;
            } else {
                return genres;
            }
        }
    })
    .filter('roundNumber', () => {
        return (nbr) => {

            return  Math.round(nbr * 10000)/100;

        }
    })
    .filter('phrasing', () => {
        return origin => phrases[origin] || origin;
    })
    .filter('colorMin', () => {
        return origin => (colors[origin] && colors[origin][0] ? colors[origin][0] : "#ffffff");
    })
    .filter('colorMax', () => {
        return origin => (colors[origin] && colors[origin][1] ? colors[origin][1] : "#ffffff");
    });