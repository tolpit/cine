
var originData = {};

angular.module('category', []).
    controller('CategoryCtrl', function ($rootScope, $scope, $filter, Movie) {
        $rootScope.state = 'home';

        $scope.years = [];
        $scope.genres = {};
        $scope.genresNames = [];
        $scope.total = 0;

        Movie.all().then((movies) => {

            movies
                .forEach(function(movie) {
                    var year = parseInt(movie.year, 10);

                    if($scope.years.indexOf(year) == -1 && !isNaN(year)) $scope.years.push(year);

                    movie.genres.forEach(function(genre) {
                        if(!$scope.genres[genre]) $scope.genres[genre] = [];

                        if($scope.genresNames.indexOf(genre) == -1) $scope.genresNames.push(genre);
                        $scope.genres[genre].push(movie);
                        $scope.total++;
                    });

                });

            originData = JSON.parse(JSON.stringify($scope.genres));

            $scope.years.sort();

            $scope.$apply();

            $rootScope.$emit('movies:set', {
                years: $scope.years
            });

            setTimeout(() => {
                $rootScope.state = 'categories';
                $rootScope.$apply();
            }, 3000);

            $rootScope.$on('movies:yearRange', (event, range) => {
                $scope.filterRange = range;
                orderMovies();
            });

            $rootScope.$on('movies:country', (event, data) => {
                $scope.filterCountry = ($scope.filterCountry && data.country == $scope.filterCountry) ? null : data.country;

                orderMovies();

                $scope.$apply();
            });

            function orderMovies() {
                var tmp = JSON.parse(JSON.stringify(originData));

                if($scope.filterRange) {
                    for(var genreName in $scope.genres) {
                        if(tmp[genreName] && Array.isArray(tmp[genreName])) {
                            $scope.genres[genreName] = tmp[genreName].filter((movie) => {
                                return movie.year >= $scope.filterRange[0] && movie.year <= $scope.filterRange[1];
                            });
                        }
                    }
                }
                else $scope.genres = tmp;

                if($scope.filterCountry) {
                    for(var genreName in $scope.genres) {
                        $scope.genres[genreName] = $scope.genres[genreName].filter((movie) => {
                            return ~movie.countries.indexOf($scope.filterCountry);
                        });
                    }
                }

                //Calc the total again
                $scope.total = 0;
                for(var genreName in $scope.genres) {
                   $scope.total += $scope.genres[genreName].length;
                }

                //$scope.$apply();
            }
        });

    });
