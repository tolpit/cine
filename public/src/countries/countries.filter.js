/**
 * Created by Théo on 25/11/2015.
 */

angular.module('countries')
    .filter('countryClass', () => {
        return (country) => {
            return country.toLowerCase().replace(' ', '').replace('-', '');
        }
    });