/**
 * Created by Théo on 25/11/2015.
 */

angular.module('countries', [])
    .directive('countries', function() {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: '/dist/views/countries/countries.html',
            controller: ($rootScope, $scope, Movie) => {

                Movie.countries().then((res) => {
                    $scope.countries = res;
                    $scope.$apply();
                });
                //Watchers
            },
            link: (scope, element, attrs) => {

            }
        }
    })
    .directive('country', function($rootScope) {
       return {
           restrict: 'A',
           link: (scope, element, attrs) => {
                element.on('click', function() {
                    $rootScope.$emit('movies:country', { country: attrs.country });
                });
           }
       }
    })
    .directive('flag', function($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                flag: '@flag'
            },
            template: `<i class="flag flag--{{ flag }}" style="background-image: url(/src/countries/flags/flag_{{ flag }}.svg);"></i>`
        }
    });