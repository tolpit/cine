
var originData = {};

angular.module('movie', []).
    controller('MovieListCtrl', function ($rootScope, $scope, $stateParams, $location, Movie) {
        $rootScope.state = 'keywords';

        $scope.years    = [];
        $scope.category = $stateParams.category;
        $scope.movies   = [];
        $scope.keywords = {};
        $scope.total = 0;

        $scope.filterRange   = $location.$$search.date;
        $scope.filterCountry = $location.$$search.country;

        if($scope.filterRange) $scope.filterRange = $scope.filterRange.replace('[', '').replace(']', '').split(',').map(date => parseInt(date, 10));

        Movie.keywords({ country: $scope.filterCountry, genre: $scope.category }).then((movies) => {

            $scope.movies = movies;

            movies.forEach((movie) => {
                //year
                var year = parseInt(movie.year, 10);

                if($scope.years.indexOf(year) == -1 && !isNaN(year)) $scope.years.push(year);

                //keywords
                if(movie.keywords) {
                    movie.keywords.forEach((keyword) => {
                        if(!$scope.keywords[keyword]) $scope.keywords[keyword] = [];

                        $scope.keywords[keyword].push(movie);
                        $scope.total++;
                    });
                }
            });

            originData = JSON.parse(JSON.stringify($scope.keywords));

            orderByDate();

            $scope.years.sort();

            $scope.$apply();

            $rootScope.$emit('movies:set', {
                years: $scope.years
            });
        });

        $rootScope.$on('movies:yearRange', (event, range) => {
            $scope.filterRange = range;
            orderByDate();
        });

        function orderByDate() {
            for(var keyword in originData) {
                if(!originData[keyword] && Array.isArray(originData[keyword])) return;

                $scope.keywords[keyword] = originData[keyword].filter((movie) => {
                    movie.year = parseInt(movie.year, 10);

                    if($scope.filterRange && (movie.year < $scope.filterRange[0] || movie.year > $scope.filterRange[1])) return false;
                    return true;
                });
            }
        }
    })
    .controller('MovieCtrl', function ($scope, $stateParams, Movie) {
        Movie.get($stateParams.id).then((movies) => {

        });
    });
