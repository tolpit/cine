/**
 * Created by Théo on 24/11/2015.
 */

var max;
var size;

angular.module('movie')
        .directive('movie', function() {
           return {
               restrict: 'A',
               link: function(scope, element, attrs) {
                   var fontSize = (attrs.size > 10 ? 10 : attrs.size) + "em";

                   var color = d3.scale
                       .linear()
                       .domain([1, 10]).range([attrs.from, attrs.to]);

                   element.css({ fontSize });

                   d3.select(element[0])
                       .transition()
                       .ease('linear')
                       .duration(200)
                       .style("fontSize", fontSize)
                       .style("color", () => {
                           return color(attrs.size);
                       });
               }
           };
        });