
angular.module('movie').
    factory('Movie', function() {
        return {

            all: () => {
                return new Promise((resolve, reject) => {
                    fetch('/api/movie/list')
                        .then(res => res.json())
                        .then(resolve)
                        .catch(reject);
                });

            },

            category: (name) => {
                return new Promise((resolve, reject) => {
                    fetch('/api/movie/category/' + name)
                        .then(res => res.json())
                        .then(resolve)
                        .catch(reject);
                });
            },

            countries: () => {
                return new Promise((resolve, reject) => {
                    fetch('/api/movie/countries')
                        .then(res => res.json())
                        .then(resolve)
                        .catch(reject);
                });
            },

            keywords: (params) => {
                return new Promise((resolve, reject) => {
                    fetch('/api/movie/keywords?country=' + params.country + '&genre=' + params.genre)
                        .then(res => res.json())
                        .then(resolve)
                        .catch(reject);
                });
            },

            get: (id) => {

            }

        }
    });