var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var MovieSchema = new Schema({
    name: String,

    updated_at: Date,
    created_at: Date
});

//Before to save the Movie
MovieSchema.pre('save', function(next) {
    this.updated_at = now;

    if(!this.created_at)
        this.created_at = now;
});

/**
 * Statics
 */
MovieSchema.statics.load = function(id, cb) {
    this.findOne({ _id: id }).exec(cb);
};

module.exports = mongoose.model('Movie', MovieSchema);