var data    = require('../data/movieList3.json');
var fs      = require('fs');
var request = require('request');

var index = 0;//Math.floor(data.movies.length * .95);
var total = 20;
var inSave = false;

groupKeywords();

function nextMovie() {
    console.log((index / total) * 100);

    if(data.movies[index] && data.movies[index].id && !data.movies[index].keywords) {
        request(`http://www.imdb.com/title/${data.movies[index].id}/keywords`, (err, response, body) => {
            if(err) console.error(err);

            if(!body) {
                console.log(data.movies[index].id);
                return;
            }

            try {
                var $ = cheerio.load(body);

                data.movies[index].keywords = [];

                $('.sodatext').find('a').each(function() {
                    var keyword = $(this).text();

                    if(keyword.split(' ').length <= 2) {
                        data.movies[index].keywords.push(keyword);
                    }
                });

                index++;

                if(index <= total && data.movies[index]) {
                    nextMovie();
                }
                else {
                    groupKeywords();
                }
            }
            catch(err) {
                console.error(err);
            }
        });
    }
    else {
        index++;
        nextMovie();
    }

}

function groupKeywords() {
    console.log('call');
    var keywords = {};

    //Create freq for keywords
    data.movies.forEach((movie) => {
        if(movie.keywords) {
            movie.keywords.forEach((key) => {
                if(!keywords[key]) keywords[key] = 1;
                else keywords[key]++;
            })
        }
    });

    //
    data.movies.forEach((movie) => {
        if(movie.keywords) {
            var toRemove = [];

            movie.keywords.forEach((key) => {
                if(keywords[key] && keywords[key] < 10) {
                    toRemove.push(key);
                }
            });

            toRemove.forEach((key) => {
                movie.keywords.splice(movie.keywords.indexOf(key), 1);
            });
        }
    });

    console.log(Object.keys(keywords).length);

    //finalSave();
}

function finalSave() {
    if(inSave) return;

    inSave = true;

    fs.writeFile('../data/movieList3.json', JSON.stringify(data, null, 4), function(err) {
        if(err) {
            console.error(err);
        } else {
            console.log("JSON saved to movieList.json");
        }
    });
    data.movies.forEach((movie) => {
        if(movie.keywords)
            console.log(movie.keywords.length);
    });
}

