var data    = require('../data/movieList.json');
var fs      = require('fs');
var request = require('request');

var index = 0;//Math.floor(data.movies.length * .95);
var total = data.movies.length;
var inSave = false;

function nextMovie() {
    console.log((index / total) * 100);

    if(data.movies[index] && data.movies[index].id && !data.movies[index].countries) {
        request(`http://www.omdbapi.com/?i=${data.movies[index].id}&plot=short&r=json`, (err, response, body) => {
            if(err) console.error(err);

            if(!body) {
                console.log(response, body);
                return;
            }

            try {
                var json = JSON.parse(body);

                if(json.Country)
                    data.movies[index].countries = json.Country.split(',').map(country => country.trim());
                else
                    data.movies[index].countries = [];

                data.movies[index].released  = json.Released;

                index++;

                if(index <= total && data.movies[index]) {
                    nextMovie();
                }
                else {
                    finalSave();
                }
            }
            catch(err) {
                console.error(err, body);
            }
        });
    }
    else {
        index++;
        nextMovie();
    }

}

function finalSave() {
    if(inSave) return;

    inSave = true;

    fs.writeFile('../data/movieList.json', JSON.stringify(data, null, 4), function(err) {
        if(err) {
            console.error(err);
        } else {
            console.log("JSON saved to movieList.json");
        }
    });
}

