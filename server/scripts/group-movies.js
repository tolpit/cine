var data    = require('../data/movieList.json');
var fs      = require('fs');

var inSave = false;

function groupMovies() {
    data.movies.forEach((movie) => {
        if(movie.countries) {
            var naIndex  = movie.countries.indexOf("N/A");

            if(~naIndex) {
                movie.countries.splice(naIndex, 1);
            }
        }
        /*var familyIndex = movie.genres.indexOf("Family");
        var crimeIndex  = movie.genres.indexOf("Crime");
        var noirIndex  = movie.genres.indexOf("Film Noir");
        var musicalIndex  = movie.genres.indexOf("Musical");
        var adventureIndex  = movie.genres.indexOf("Adventure");
        var biographyIndex  = movie.genres.indexOf("Biography");
        var fantasyIndex  = movie.genres.indexOf("Fantasy");
        var talkIndex = movie.genres.indexOf("Talk-Show");
        var gameIndex = movie.genres.indexOf("Game-Show");
        var realIndex = movie.genres.indexOf("Reality-TV");

        if(~talkIndex) movie.genres.splice(talkIndex, 1);
        if(~gameIndex) movie.genres.splice(gameIndex, 1);
        if(~realIndex) movie.genres.splice(realIndex, 1);
        var germanIndex  = movie.countries.indexOf("West Germany");*/

        /*if(~familyIndex) {
            movie.genres.splice(familyIndex, 1);

            if(movie.genres.indexOf("Comedy") == -1)
                movie.genres.push("Comedy");
        }

        if(~crimeIndex) {
            movie.genres.splice(crimeIndex, 1);

            if(movie.genres.indexOf("Thriller") == -1)
                movie.genres.push("Thriller");
        }*/

        /*if(~musicalIndex) {
            movie.genres.splice(musicalIndex, 1);

            if(movie.genres.indexOf("Music") == -1)
                movie.genres.push("Music");
        }

        if(~adventureIndex) {
            movie.genres.splice(adventureIndex, 1);

            if(movie.genres.indexOf("Action") == -1)
                movie.genres.push("Action");
        }

        if(~biographyIndex) {
            movie.genres.splice(biographyIndex, 1);

            if(movie.genres.indexOf("History") == -1)
                movie.genres.push("History");
        }

        if(~fantasyIndex) {
            movie.genres.splice(fantasyIndex, 1);

            if(movie.genres.indexOf("Mystery") == -1)
                movie.genres.push("Mystery");
        }*/
    });

    finalSave();
}

function finalSave() {
    if(inSave) return;

    inSave = true;

    fs.writeFile('../data/movieList.json', JSON.stringify(data, null, 4), function(err) {
        if(err) {
            console.error(err);
        } else {
            console.log("JSON saved to movieList.json");
        }
    });
}

groupMovies();