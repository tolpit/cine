var fs = require('fs');
var cheerio = require('cheerio');
var request = require('request');

const PER_PAGE = 50;
const TOTAL    = 6899;

var tmp = { movies: [] };
var done = 0;
var page = 0;
var inSave = false;

function fetch() {
    console.log(page);
    var uri = `http://www.imdb.com/search/title?locations=Paris,%20France&ref_=ttloc_loc_10&start=${page}`;

    request({ uri }, function(err, response, body) {
        if(!body) return;

        var $ = cheerio.load(body);

        $('td.title').each(function() {
            var element = $(this);

            var year = element.find('.year_type').text().replace('(', '').replace(')', '');

            if(year.indexOf('TV Series') == -1) {
                var id = element.find('.wlb_wrapper').attr('data-tconst');
                var title = element.find('a').first().text();
                var genres = [];

                year = parseInt(year, 10);

                element.find('.genre').find('a').each(function() {
                    genres.push( $(this).text() );
                });

                tmp.movies.push({ id, title, genres, year });

                done++;
            }
        });

        if(page <= TOTAL) {
            page += PER_PAGE;
            fetch();
        }
        if((page >= TOTAL) && !inSave) saveData();
    });
}

fetch();

function saveData() {
    inSave = true;

    fs.writeFile('../data/movieList.json', JSON.stringify(tmp, null, 4), function(err) {
        if(err) {
            console.error(err);
        } else {
            console.log("JSON saved to movieList.json");
        }
    });
}