var data    = require('../data/movieList.json');
var fs      = require('fs');

var inSave = false;

function groupMovies() {
    data.movies.forEach((movie) => {
        var year = parseInt(movie.year, 10);

        if(movie.year != year.toString()) {
            movie.year = year;
        }

    });

    finalSave();
}

function finalSave() {
    if(inSave) return;

    inSave = true;

    fs.writeFile('../data/movieList.json', JSON.stringify(data, null, 4), function(err) {
        if(err) {
            console.error(err);
        } else {
            console.log("JSON saved to movieList.json");
        }
    });
}

groupMovies();