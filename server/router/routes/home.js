var express = require('express');
var router  = express.Router();

/* Home page. */
router.get('/', function(req, res) {
    res.render('index');
});

router.get('/category/:category', function(req, res) {
    res.render('index');
});

router.get('/movie/:movie', function(req, res) {
    res.render('index');
});

module.exports = router;