var express     = require('express');
var router      = express.Router();
var MovieCtrl   = require('../../controllers/movieCtrl');

router.get('/api/movie/list', MovieCtrl.list);

router.get('/api/movie/category/:categoryName', MovieCtrl.category);

router.get('/api/movie/countries/:countryName?', MovieCtrl.countries);

router.get('/api/movie/keywords', MovieCtrl.keywords);

module.exports = router;
