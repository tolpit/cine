var express  = require('express');
var router   = express.Router();

router.get('/404', function(req, res) {
    res.status(404);

    res.render('error/base', {
        message: "Page introuvable"
    });
});

router.get('/401', function(req, res) {
    res.status(401);

    res.render('error/base', {
        message: "Accès non autorisé"
    });
});


module.exports = router;