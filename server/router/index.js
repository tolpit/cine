var express = require('express');
var router  = express.Router();

module.exports = function (app) {
    app.use('/', require('./routes/home'));
    app.use('/', require('./routes/errors'));
    app.use('/', require('./routes/movie'));
};
